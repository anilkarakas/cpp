#include <iostream>
#include <vector>
using namespace std;

class Student{
   
   public:
      vector <int> scores = {0, 0, 0, 0, 0};
   
      Student create_student(){
         Student student;
         for(int i = 0; i < 5; i++){
            int input;
            cin >> input;
            scores[i] = input;
         }
         return student;
      }
      
      int calculateTotalScore(){
         int sum = 0;
         vector<int>::iterator iter;
         for(iter = scores.begin(); iter < scores.end(); iter++){
            sum += *iter;      
         }
         return sum;
      }
};

int main(){
   int num_stu;
   cin >> num_stu;
   Student random_student;
   vector<int> sum_stu;
   for(int i = 0; i < num_stu; i++){
      random_student.create_student();
      sum_stu.push_back(random_student.calculateTotalScore());
   }
   int higher_stu = 0;
   for(int i = 1; i < num_stu; i++){
      if(sum_stu[0] < sum_stu[i]){
         higher_stu += 1;
      }
   }
   cout << higher_stu << endl;
   for(int i = 0; i < num_stu; i++){
      cout << sum_stu[i] << ' ';
   }
   return 0;
}