#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int n;
    std::cin >> n;
    std::vector<int> v(n); 
    for(int i = 0; i < n; i++){
        int dummy;
        std::cin >> dummy;
        v.at(i) = dummy;
    }
    sort(v.begin(),v.end());
    for(const auto &vit : v){
        std::cout << vit << ' ';
    }
    return 0;
}