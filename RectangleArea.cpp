#include <iostream>

using namespace std;
 
class Rectangle{
    private:
        int width;
        int height;
    public:
        Rectangle(){};
        virtual void display(){
            std::cout << width << ' ' << height << std::endl;
        }
    friend class RectangleArea;
};

class RectangleArea : public Rectangle {
    private:
        
    public:
        RectangleArea() 
        : Rectangle() {
        }
        
        void read_input(){
            std::cin >> width >> height;
        }
    
        void display() override {
            int area = width*height;
            std::cout << area  << std::endl;
        }
};


int main()
{
    /*
     * Declare a RectangleArea object
     */
    RectangleArea r_area;
    
    /*
     * Read the width and height
     */
    r_area.read_input();
    
    /*
     * Print the width and height
     */
    r_area.Rectangle::display();
    
    /*
     * Print the area
     */
    r_area.display();
    
    return 0;
}